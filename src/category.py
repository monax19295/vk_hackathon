from src.entity.tag_entity import get_category_for_word


def find_category(text):
    return get_category_for_word(text.split(' '))