import pymorphy2
import nltk.stem
from nltk.corpus import stopwords

alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
s = nltk.stem.SnowballStemmer('russian')
morph = pymorphy2.MorphAnalyzer()
prepare_func = []
stop_words = stopwords.words('russian')


def text_stemming(text):
    return s.stem(text)


def text_remove_stop_words(text):
    #print('\nbefore stop words ' + text)
    text = ' '.join([word for word in text.split(' ') if word not in stop_words])
    #print('\nafter stop words ' + text)
    return text


def text_to_lower(text):
    return text.lower()


def text_letter_only(text):
    new_text = ' '
    # is prev a space
    prev = False
    for letter in text:
        if letter in alphabet:
            new_text += letter
            prev = False
            continue
        elif new_text[-1] != ' ':
                new_text += ' '
    return new_text.replace('ё', 'е').strip()


def text_lemmatization(text):
    lem_text_array = []
    for word in text.split(' '):
        lem_text_array.append(morph.parse(word)[0].normal_form)
    return ' '.join(lem_text_array)


def apply_func(text, func_array):
    for func in func_array:
        text = func(text)
    return text


# manage all preparing in one function
def prepare_text(text):
    return apply_func(text, prepare_func)

def prepare_array(texts):
    prepare_func = [
        text_to_lower,
        text_letter_only,

        text_remove_stop_words,
        # text_stemming,
        text_lemmatization,
    ]
    #new_texts = []
    #for text in texts:
        #new_texts.append(apply_func(text, prepare_func))
    return apply_func(texts, prepare_func)