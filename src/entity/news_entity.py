import uuid

from sqlalchemy.dialects.postgresql import UUID

from src.db import db
from src.entity.tag_entity import Tags
from src.keywords import get_keywrods


class News(db.Model):
    id = db.Column(UUID(as_uuid=True),
                primary_key=True)
    __table_args__ = {'schema': 'vk'}
    category = db.Column(db.String)
    header = db.Column(db.String)
    anons = db.Column(db.String)
    article = db.Column(db.String)
    tags = db.Column(db.String)

    def __init__(self,
                 category,
                 header,
                 anons,
                 article,
                 tags):
        self.id = uuid.uuid4()
        self.category = category
        self.header = header
        self.anons = anons
        self.article = article
        self.tags = tags


def update_keywords():
    all_news = News.query.all()
    # print(list(all_news))
    # print(' '.join(all_news))
    for news in list(all_news):
        text = news.header + news.anons + news.article
        keywords = get_keywrods(text, count=10)
        news.tags = keywords
        for key in keywords:
            t = Tags(news.category, key, 0.5)
            db.session.add(t)
            print('add tag ' + key)
        # db.session.query(News).update(news)
        db.session.commit()
        print('add news ' + str(news.id))
        # news.tags = list(keywords.keys())