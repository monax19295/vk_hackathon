import uuid

from src.db import db
from sqlalchemy.dialects.postgresql import UUID

class Tags(db.Model):
    id = db.Column(UUID(as_uuid=True),
                primary_key=True)
    __table_args__ = {'schema': 'vk'}
    category = db.Column(db.String)
    tag = db.Column(db.String)
    freq = db.Column(db.Numeric(10, 4))

    def __init__(self,
                 category,
                 tag,
                 freq):
        self.id = uuid.uuid4()
        self.category = category
        self.tag = tag
        self.freq = freq

def get_category_for_one_word(word):
    r = db.session.query(Tags).filter(Tags.tag == word).all()
    if r is None or len(list(r)) == 0:
        return ""

    freq = {}
    for item in list(r):
        if item.category not in freq:
            freq[item.category] = 0
        else:
            freq[item.category] += 1

    count = max(list(freq.values()))
    for k, v in freq.items():
        if v == count:
            return k
    return r.category


def get_category_for_word(words):
    '''
    один запрос, фильтры питона
    :param words:
    :return:
    '''
    r = db.session.query(Tags).filter(Tags.tag.in_(words)).all()
    l = list(r)
    if r is None or len(l) == 0:
        return ""


    words = {}
    for tag in l:
        if tag.tag not in words:
            words[tag.tag] = {tag.category: 1}
        else:
            if tag.category not in words[tag.tag]:
                words[tag.tag][tag.category] = 1
            else:
                words[tag.tag][tag.category] += 1

    result = {}
    for word in words:
        max_count = 0
        max_cat = ''
        for category, count in words[word].items():
            if count > max_count:
                max_count = count
                max_cat = category
        result[word] = max_cat

    return result
    # freq = {}
    # for item in list(r):
    #     if item.category not in freq:
    #         freq[item.category] = 0
    #     else:
    #         freq[item.category] += 1
    #
    # count = max(list(freq.values()))
    # for k, v in freq.items():
    #     if v == count:
    #         return k
    # return r.category