from flask import Flask

from src.summ import summ
from src.synonim import synonim

app = Flask(__name__)
from src.db import init
init(app)
from src.category import find_category

from flask import jsonify
import pandas as pd

from sklearn.externals import joblib
from flask import Response

# from src import app
from flask import request, json
from src.cleaning_data_service import prepare_array
from src.keywords import get_keywrods

clf = joblib.load('../resource/f_model_80.sav')
# clf = joblib.load('../resource/f_model_10.sav')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:secret@192.168.8.102:5432/vk'
from src.entity.news_entity import update_keywords

@app.route("/")
def hw():
    return "hello world"

@app.route("/up")
def update_keyw():
    """
    Выделяет теги из текстов в бд
    :return:
    """
    update_keywords()
    return "hello world123123"

@app.route("/keywords", methods=['POST'])
def keywords():
    """
    Возвращает теги для текста
    :return:
    """
    data = request.data
    data_dict = json.loads(data)
    clean_text = prepare_array(data_dict['text'])
    result = get_keywrods(clean_text, int(data_dict['count']))
    return jsonify(result)

@app.route("/category", methods=['POST'])
def catego():
    """
    Возвращает категории для текста
    :return:
    """
    data = request.data
    data_dict = json.loads(data)
    clean_text = prepare_array(data_dict['text'])
    result = find_category(clean_text)
    return jsonify(fix_lem(data_dict['text'], result))

@app.route("/synonim", methods=['POST'])
def synonim_ctrl():
    """
    Возвращает синонимы для списка слов
    :return:
    """
    data = request.data
    data_dict = json.loads(data)
    # result = synonim(data_dict['words'])

    not_clean_text = prepare_array(data_dict['text'])
    result = synonim(not_clean_text.split(' '))
    result = {i:result[i] for i in result if len(result[i]) != 0}
    return jsonify(fix_lem(data_dict['text'], result))

def fix_lem(orig_text, result_dict):
    """
    Заменяет очищенное слово его оригинальным вариантом.
    Нужно чтобы клиент мог по слову сабстрингом найти его в ориг тексте
    :param orig_text:
    :param result_dict:
    :return:
    """
    for word in orig_text.split(' '):
        clean_word = prepare_array(word)
        if clean_word in result_dict:
            v = result_dict.pop(clean_word)
            # print("remove " + word + " " + str(v))
            result_dict[word] = v
    return result_dict

@app.route("/summ", methods=['POST'])
def sum1():
    """
    Возвращает саммарайз(краткое описание) для текста
    :return:
    """
    data = request.data
    data_dict = json.loads(data)
    result = summ(data_dict['text'])
    return jsonify(result)

@app.route("/predict", methods=['POST'])
def predict_text():
    """
    Предсказывает эмоциональную окраску текста
    :return:
    """
    data = request.data
    data_dict = json.loads(data)
    text = data_dict["text"]
    cleared_text = prepare_array(text)
    # v = clf.predict_proba(cleared_text.split(" "))
    # print(clf.classes_)
    # print(v)
    words = cleared_text.split(" ")
    v = pd.DataFrame(clf.predict_proba(words), columns=clf.classes_)
    print(v)
    r = json.loads(v.to_json(orient='index'))
    dict = {}

    print(r)
    for word in r:
        w = r[word]
        for cat in w:
            print(w)
            value = w[cat]
            if cat not in dict:
                dict[cat] = 0
            else:
                dict[cat] += value
    dict1 = {"mapOfPrediction": dict, "wordPred": r}
    resp = Response(json.dumps(dict1)) #here you could use make_response(render_template(...)) too
    resp.headers['content-type'] = 'application/json'
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)