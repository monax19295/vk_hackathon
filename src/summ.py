from summa.summarizer import summarize

def summ(text):
    short = ''
    ratio = 0.01
    for r in range(1, 40):
        ratio += 0.02
        short = summarize(text, ratio=ratio, language='russian')
        if len(short) > 0:
            break
    result = {
        "prev_len": len(text),
        "new_len": len(short),
        "summ": short
    }
    return result