import gensim
import gensim.downloader as api


model = api.load("word2vec-ruscorpora-300")

# info = api.info()  # show info about available models/datasets
# model = api.load("glove-twitter-25")  # download the model and return as object ready for use
# print(model.most_similar("машина"))

# model = gensim.models.KeyedVectors.load_word2vec_format('./resource/ruscorp.zip', binary=True)
# model = gensim.models.KeyedVectors.load_word2vec_format('./resource/model.hdf5')
def synonim(positive_words):
    result = {}
    list_syn = []
    from src.entity.tag_entity import get_category_for_word
    for word in positive_words:
        l = []
        noun = calc_syn(u''+word + '_' + 'NOUN')
        verb = calc_syn(u''+word + '_' + 'VERB')
        # req_list = []
        # for tup in noun:
        #     print(tup)
        #     req_list.extend(tup)
        # for tup in verb:
        #     req_list.extend(tup)

        # print(req_list)
        # if len(req_list) != 0:
        #     result.update({word: get_category_for_word(req_list)})

        if len(noun) > 0:
            l.extend(noun)
        if len(verb) > 0:
            l.extend(verb)
        result.update({word: l})
        # for item in l:
        #     list_syn.extend(item.keys())
    # print(' '.join(list_syn[:100]))
    list_for_req = []
    for item in result.values():
        list_for_req.extend(item)
    category = get_category_for_word(list_for_req)
    res_copy = {}
    for word, synon in result.items():
        dd = {}
        for s in synon:
            if s in category:
                dd[s] = category[s]
                # res_copy[word].append({s:category[s]})
            else:
                dd[s] = ""
                # res_copy[word].append({s:""})
        res_copy[word] = dd
    # print(category)

    return res_copy

def calc_syn(positive_word):
    try:
        v = model.most_similar(positive_word)
        # print(v)
        # return v
        def c(d):
            from src.entity.tag_entity import get_category_for_one_word
            return str(d[0]).split('_')[0]

        return list(map(c, v))
    except Exception:
        pass
        # print("OOPS " + str(Exception))
    return []

# print(synonim(["пожар", "бежать", "ураган"]))
# print(model.most_similar('пожар'))